## [1.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-home/compare/v1.0.1...v1.0.2) (2024-06-06)


### Bug Fixes

* **opendesk-home:** Only redirect on / to home ([762e676](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-home/commit/762e676ae588c329bbd1dc89aef1b3bb9ef9ed04))

## [1.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-home/compare/v1.0.0...v1.0.1) (2024-04-16)


### Bug Fixes

* **opendesk-home:** Add missing tls default variables ([c9ce42a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-home/commit/c9ce42aeface61ee3e47675d8f1722005386394f))

# 1.0.0 (2024-04-16)


### Features

* **opendesk-home:** Initial commit ([6ac24a9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-home/commit/6ac24a9cb08406d4d2c8278c61de204f6659a91b))

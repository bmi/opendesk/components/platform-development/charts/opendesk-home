<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->
# openDesk Home Helm Chart

This repository contains a Helm chart which is deployed on the root of each deployment.


## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+


## Documentation

The documentation is placed in the README of each helm chart:

- [opendesk-home](charts/opendesk-home)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH

<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->
# opendesk-home

A Helm chart deploying into the root domain of each deployment

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository
```console
helm repo add opendesk-home https://gitlab.opencode.de/api/v4/projects/2784/packages/helm/stable
helm install my-release --version 1.0.2 opendesk-home/opendesk-home
```

### Install via OCI Registry
```console
helm repo add opendesk-home oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-home
helm install my-release --version 1.0.2 opendesk-home/opendesk-home
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry.opencode.de/bmi/opendesk/components/external/charts/bitnami-charts | common | 2.14.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| additionalAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| additionalLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| global.domain | string | `"example.com"` | The domain name which is used in f.e. in Ingress component. |
| global.hosts.univentionManagementStack | string | `"portal"` | Subdomain for Univention Management Stack, results in "https://{{ univentionManagementStack }}.{{ domain }}". |
| ingress.annotations | list | `[]` | Define custom ingress annotations. |
| ingress.ingressClassName | string | `"nginx"` | The Ingress controller class name. |
| ingress.tls | object | `{"enabled":true,"secretName":""}` | Secure an Ingress by specifying a Secret that contains a TLS private key and certificate.  Ref.: https://kubernetes.io/docs/concepts/services-networking/ingress/#tls |
| ingress.tls.enabled | bool | `true` | Enable TLS/SSL/HTTPS for Ingress. |
| ingress.tls.secretName | string | `""` | The name of the kubernetes secret which contains a TLS private key and certificate. Hint: This secret is not created by this chart and must be provided. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License
This project uses the following license: Apache-2.0

## Copyright
Copyright (C) 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
